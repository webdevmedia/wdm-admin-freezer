<?php # -*- coding: utf-8 -*-
/**
 * Plugin Name: WDM Admin Freezer
 * Description: Freeze your AdminInterface, prevent your author enter the WordPress backend
 * Author:      web dev media UG
 * Author URI:  http://www.web-dev-media.de
 * Version:     1.0.0
 * Text Domain: wdmAdminFreezer
 * License:     GPLv3
 */
namespace wdmAdminFreezer;

use wdmAdminFreezer\Handler\pluginConfig;

add_action( 'plugins_loaded', function () {

	require_once( dirname( __FILE__ ) . '/inc/autoloader.php' );

	new Autoloader( __FILE__ );

	$config = new pluginConfig();

	$config->add('plugin', [
		'baseDir' => dirname( __FILE__ ) . '/',
		'baseUrl' => plugin_dir_url( __FILE__ ),
	]);

    $baseDir = $config->getSetting( 'plugin', 'baseDir');
    $baseUrl = $config->getSetting( 'plugin', 'baseUrl');

	$config->add('plugin', [
		'assets' => $baseDir . 'assets/',
		'vendor' => $baseDir . 'vendor/',
		'name' => get_file_data(__FILE__, ['Plugin Name' => 'Plugin Name',], 'plugin')['Plugin Name'],
		'version' => get_file_data(__FILE__, ['Version' => 'Version',], 'plugin')['Version'],
		'namespace' => __NAMESPACE__
	]);

	$config->add('enqueue', [
		'styles' => [
			'pluginAdminCss' => [
				'src'       => 'assets/css/general-admin.css',
				'deps'      => NULL,
				'version'   => NULL,
				'media'     => NULL,
				'url'       => $baseUrl,
				'use@'      => 'admin',
			],
            'pluginMainCss' => [
                'src'     => 'assets/css/general.css',
                'deps'    => NULL,
                'version' => NULL,
                'media'   => NULL,
                'url'     => $baseUrl,
            ]
		],
		/*'scripts' => [
				['src' => $baseDir . 'assets/css/foo.js']
			],*/
	]);

	$pluginConfig = $config;

	Factories\Module::initModule($pluginConfig);

	$modules = Factories\Module::getModules();

	#load_plugin_textdomain( 'DellEmailCampaigns', false,       $config->getSetting('plugin', 'assets') . '/assets/languages' );
} );
