<?php
namespace wdmAdminFreezer\Handler;

class View  {

	private static $folders = 'View';
	private static $arguments = [];

	public static function get(string $tpl, $arguments = []): string{
		$paths = $tpl;
		$template = '';

		if(!file_exists($paths)) {
			$fileName = ucfirst( $tpl ) . '.php';
			$paths    = STRUCTURED_CONTENT_PLUGIN_BASEDIR . 'inc/' . self::$folders . '/' . $fileName;

			$callee = Common::getCallee( 3 );

			if ( ! empty( $callee ) ) {
				$moduleTemplate = $callee[ 'moduleBasePath' ] . 'View/' . $fileName;

				if ( file_exists( $moduleTemplate ) ) {
					$paths = $moduleTemplate;
				}
			}

			$template = '<p><b>Missing template where searched in:</b> <ul><li>' . $paths . '</li><li>' . $moduleTemplate . '</li></ul></p>';
		}

		self::$arguments = $arguments;

		if(file_exists($paths)) {
			ob_start();
			require_once $paths;
			$template = ob_get_contents(); // get contents of buffer
			ob_end_clean();
		}

		return $template;
	}

	public static function display(string $tpl, $arguments = []){
		echo self::get($tpl, $arguments);
		return null;
	}

	public static function getArguments(){
		return self::$arguments;
	}

	public static function enqueScript($script){
		if(!empty($script)){
			$suffix = enqueueScripts::get_script_suffix();
			$handle = DellEmailCampains_BASENAMESPACE . '_' . $script;

			/* @var $wp_scripts \WP_Scripts*/
			$wp_scripts = wp_scripts();
			$wp_scripts->in_footer[] = $handle;
			$wp_scripts->add(
				$handle,
				STRUCTURED_CONTENT_PLUGIN_URL . 'assets/js/' . $script . (!empty($suffix) ? $suffix . '.' : '') . '.js',
				'',
				'1.0.0',
				TRUE
			);

			$wp_scripts->do_item($handle);
		}
	}

}
