<?php
namespace wdmAdminFreezer\Handler;

use wdmAdminFreezer\Factories;

class Module  {

	public static function getModule($moduleName){
		$modules = Factories\Module::getModules();

		if(!strpos( $moduleName, 'Module')){
			$moduleName = 'Module\\' . $moduleName;
		}

		try {
			if(!empty($modules[$moduleName])) {
				return $modules[$moduleName];
			}else{
				throw new \Exception("Module: " . $moduleName . " not found!");
			}

		} catch (\Exception $e) {
			echo '<pre>';print_r( [ 'Location' => [ 'PATH' => dirname( __FILE__ ), 'FILE' => basename( __FILE__ ), 'FUNCTION' => __FUNCTION__ . ':' . __LINE__ ], 'Exception' => [
				'msg' => $e->getMessage(),
				'$moduleName' => $moduleName
			]] );
			die();
		}
	}

	public static function getModules(){
		return Factories\Module::getModules();
	}

}
