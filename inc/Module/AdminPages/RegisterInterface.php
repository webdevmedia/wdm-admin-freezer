<?php # -*- coding: utf-8 -*-

namespace wdmAdminFreezer\Module\AdminPages;

/**
 * Interface register_setting_pages_interface
 */
interface RegisterInterface {

	/**
	 * @return string
	 */
	public function init(array $configs);

}
