<?php # -*- coding: utf-8 -*-
namespace wdmAdminFreezer\Module\AdminPages\Callbacks;

use wdmAdminFreezer\Module\AdminPages\FormElements;

class adminFreeze {

    public function displayPage($pageSettings){
	  	?>
			<div class="wrap">
				<h1><?=$pageSettings['title']?></h1>

				<form method="post" action="<?=$pageSettings['parent_menu_slug']?>?page=<?=$pageSettings['menu_slug']?>">
					<?php do_settings_sections($pageSettings['menu_slug']); ?>
					 <?php submit_button(); ?>
				</form>
			</div>
		<?php
    }

    public static function nonce($fieldSettings){
	    if (!empty($_POST[$fieldSettings['id'] ])) {
		    update_option($fieldSettings['id'] , $_POST[$fieldSettings['id'] ]);
	    }

	    FormElements::nonce($fieldSettings['id'],$fieldSettings['nonceString']);

		# echo '<input name="' . $fieldSettings['id'] . '" id="' . $fieldSettings['id'] . '" placeholder="' . $fieldSettings['placeholder'] . '" type="' . $fieldSettings['type'] . '" value="' . get_option( $fieldSettings['id']  ) .'" class="' . $fieldSettings['class'] . '" styles="' . self::getStylesStr($fieldSettings['styles']). '" />';

    }

    private function getStylesStr(array $styles): string{
    	$sty = [];

    	if(!empty($styles)) {
		    foreach ( $styles as $attr => $value ) {
			    $sty[] = $attr . ':' . $value . ';';
		    }
	    }

    	return implode($sty);
    }

}
