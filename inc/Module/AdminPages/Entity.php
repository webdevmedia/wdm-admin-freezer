<?php
namespace wdmAdminFreezer\Module\AdminPages;

use wdmAdminFreezer\Factories;

class Entity extends Factories\Abstracts\Entity  {

	public function __construct() {
		$this->load();
	}

	public function load(){

		$this->loadAdminPagesConfigs();
        $entities = $this->get();

        (new Register())->init($entities);
	}

	private function loadAdminPagesConfigs(){
		$boxPath = dirname(__FILE__) . '/configs';
		$this->loadEntities($boxPath, 'json', true);
	}

}
