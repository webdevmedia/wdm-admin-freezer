<?php

namespace wdmAdminFreezer\Module\AdminPages;

/**
 * Build the markup for a formular
 *
 * @package metabox
 */
class FormElements {

	/**
	 * Get inline style-tag for a single formfield
	 *
	 * @return string
	 */
	public static function nonce(string $id, string $str) {
		echo  '<input type="hidden" name="' . $id . '" value="' . wp_create_nonce( $str ) . '" />';
	}

	/**
	 * Get inline style-tag for a single formfield
	 *
	 * @return string
	 */
	public static function get_field_style() {

		if ( ! empty( $this->field[ "style" ] ) ) {
			return ' style="' . $this->field[ "style" ] . '" ';
		}

	}

	/**
	 * Adds custom class at the inner formfield wrapper div
	 *
	 * @return string
	 */
	public static function get_wrapper_class() {

		if ( ! empty( $this->field[ "class" ] ) ) {
			return $this->field[ "class" ] . ' ';
		}

	}

	/**
	 * Adds id at the inner formfield wrapper div
	 *
	 * @return string
	 */
	public static function get_wrapper_id() {

		if ( ! empty( $this->fieldset[ "id" ] ) ) {
			return ' id="' . $this->fieldset[ "id" ] . '_' . $this->iteration . '"';
		}

	}

	/**
	 * Set the fieldname hirachie
	 *
	 * @param $fieldset
	 *
	 * @return string
	 */
	public static function get_field_id( $fieldset ) {

		if ( ! empty( $fieldset[ "id" ] ) ) {
			return '[' . $fieldset[ "id" ] . ']';
		}

	}

	public static function get_id( $_param ) {

		$id = FALSE;

		if ( array_key_exists( 'id', $_param[ 'field' ] ) ) {
			$id = 'id="' . $_param[ 'field' ][ "id" ] . '" ';
		}

		return $id;

	}

	public static function get_lable_id( $_param ) {

		$id = FALSE;

		if ( array_key_exists( 'label', $_param[ 'field' ] ) ) {
			$id = 'id="' . $_param[ 'field' ][ "label" ] . '" ';
		}

		return $id;

	}

	public static function get_input_content_type( $_param ) {

		$type = 'text';

		if ( ! empty( $_param[ 'field' ][ 'content-type' ] ) ) {
			$type = $_param[ 'field' ][ 'content-type' ];
		}

		return $type;
	}

	public static function get_attributes( $_param ) {

		$attributes = FALSE;

		if ( array_key_exists( 'attributes', $_param[ 'field' ] ) ) {

			foreach ( $_param[ 'field' ][ 'attributes' ] as $attribute => $value ) {

				$attributes .= $attribute . '="' . $value . '" ';

			}

		}

		return $attributes;

	}

	/**
	 * Returns button markup
	 *
	 * @see wp-admin/includes/template-functions.php:1828
	 *
	 * @param $_param array
	 *
	 * @return string
	 */
	public static function button( $_param ) {

		return \get_submit_button( $_param[ 'field' ][ 'text' ], $_param[ 'field' ][ 'class' ],
		                           $_param[ 'field' ][ 'name' ], $_param[ 'field' ][ 'wrap' ],
		                           $_param[ 'field' ][ 'attributes' ] );
	}

	public static function anker( $_param ) {

		$defaults = array(
			'href' => '#',
			'text' => 'link',
		);

		$attributes = FALSE;

		$attr = wp_parse_args( $_param[ 'field' ], $defaults );

		foreach ( $attr as $attr_name => $attr_val ) {

			$attributes[] = $attr_name . '="' . $attr_val . '"';

		}

		return '<a ' . implode( ' ', $attributes ) . '>' . $attr[ 'text' ] . '</a>';
	}

	/**
	 * Returns textarea markup
	 *
	 * @param $_param
	 *
	 * @return string
	 */
	public static function textarea( $_param ) {
		$value    = $this->get_value( $_param[ 'field_id' ], $_param[ 'field_name' ] );
		$lable_id = $this->get_lable_id( $_param );

		return '<textarea  name="' . $this->prefix . $_param[ 'field_id' ] . '[' . $_param[ 'field_name' ] . ']"' . $lable_id . $this->get_attributes( $_param ) . $this->get_field_style($_param) . '>' . $value . '</textarea>';
	}

	/**
	 * Returns input markup
	 *
	 * @param $_param array
	 *
	 * @return string
	 */
	public static function input( $_param ) {

		$value    = $this->get_value( $_param[ 'field_id' ], $_param[ 'field_name' ] );
		$lable_id = $this->get_lable_id( $_param );

		$type = $this->get_input_content_type( $_param );

		$maxmin = FALSE;

		if ( $type == 'number' && array_key_exists( 'max', $_param[ 'field' ] )
		     && array_key_exists( 'min', $_param[ 'field' ] )
		) {
			$maxmin = "max='" . $_param[ 'field' ][ 'max' ] . "' min='" . $_param[ 'field' ][ 'min' ] . "' ";
		}

		$input = "<input " . $maxmin . $lable_id . "value='" . $value . "' type='" . $type . "' name='" . $this->prefix . $_param[ 'field_id' ] . "[" . $_param[ 'field_name' ] . "]' " . $this->get_id( $_param ) . $this->get_attributes( $_param ) . "/>";

		if ( $this->metadata && $type == 'file' && array_key_exists( 'upload', $this->metadata ) ) {

			if ( $this->metadata[ 'upload' ][ 'count' ] > 0 ) {

				$input .= '<div class="csvParsed">' . $this->metadata[ 'upload' ][ 'count' ] . ' Einträge vorhanden. <span id="newCSV">Daten löschen</span></div>';

			}

			return $input;

		} else {

			return $input;

		}

	}

	/**
	 * Returns description markup
	 *
	 * @param $_param
	 *
	 * @return string
	 */
	public static function description( $_param ) {

		if ( ! empty( $field[ 'description' ] ) ) {
			$desc_style = FALSE;

			if ( $_param[ 'type' ] == 'radio' || $_param[ 'type' ] == 'checkbox' ) {
				$desc_style = ' style="margin-top: -11px;display: block;" ';
			}

			return '<br /><span class="description"' . $desc_style . '>' . $_param[ 'description' ] . '</span>';
		}

	}

	/**
	 * Returns options markup
	 *
	 * @param $_param
	 *
	 * @return bool|string
	 */
	public static function options( $_param ) {

		if ( isset( $_param[ 'field' ][ 'options' ] ) ) {

			$output = FALSE;

			foreach ( $_param[ 'field' ][ 'options' ] as $key => $option ) {

				$value = $this->get_value( $_param[ 'field_id' ], $_param[ 'field_name' ] );

				$description = FALSE;

				if ( is_array( $option ) ) {
					$option      = $_param[ 'field' ][ 'options' ][ $key ][ 'option' ];
					$description = $_param[ 'field' ][ 'options' ][ $key ][ 'description' ];
				}

				$options[ $key ][ 'input' ] = array(
					'type'    => $_param[ 'field' ][ 'type' ],
					'id'      => $this->prefix . $_param[ 'field_id' ] . '[' . $key . ']',
					'value'   => $option,
					'attr'    => $this->get_attributes( $_param ),
					'checked' => FALSE
				);

				$options[ $key ][ 'label' ] = array(
					'for'  => $this->prefix . $_param[ 'field_id' ] . '[' . $key . ']',
					'text' => $option
				);

				if ( $_param[ 'field' ][ 'type' ] == 'checkbox' ) {
					$name = $this->prefix . $_param[ 'field_id' ] . '[' . $_param[ 'field_name' ] . '][' . $key . ']';

					if ( isset( $this->metadata[ $_param[ 'field_name' ] ][ $key ] ) ) {
						$options[ $key ][ 'input' ][ 'checked' ] = 'checked="checked"';
					}

				} else {
					$name                                    = $this->prefix . $_param[ 'field_id' ] . '[' . $_param[ 'field_name' ] . ']';
					$options[ $key ][ 'input' ][ 'checked' ] = checked( $value, $option, FALSE );
				}

				$options[ $key ][ 'input' ][ 'name' ] = $name;

			}

			foreach ( $options as $key => $option ) {

				$input = $options[ $key ][ 'input' ];
				$label = $options[ $key ][ 'label' ];

				if ( empty( $input[ 'checked' ] )
				     && array_key_exists( 'checked', $_param[ 'field' ][ 'options' ][ $key ] )
				) {

					$input[ 'checked' ] = 'checked="checked"';

				}

				$output .= '<input type="' . $input[ 'type' ] . '"' . $input[ 'checked' ] . 'name="' . $input[ 'name' ] . '" id="' . $input[ 'id' ] . '" value="' . $input[ 'value' ] . '" ' . $input[ 'attr' ] . '/>';
				$output .= '<label for="' . $label[ 'for' ] . '" >' . $label[ 'text' ] . ' </label>';

				if ( ! empty( $description ) ) {
					$output .= '<div>' . $description . '</div>';
				}
			}

			return $output;
		}

	}

	/**
	 * Returns select markup
	 *
	 * @param $_param
	 *
	 * @return string
	 */
	public static function select( $_param ) {

		$select = '<select name="' . $this->prefix . $_param[ 'field_id' ] . '[' . $_param[ 'field_name' ] . ']" ' . $_param[ 'style' ] . '>';

		foreach ( $_param[ 'field' ][ 'options' ] as $option ) {
			$select .= '<option value="' . $option . '"' . selected( $this->metadata[ $_param[ 'field_name' ] ],
			                                                         $option, FALSE ) . '>' . $option . '</option>';
		}

		$select = '</select>';

		return $select;

	}

	/**
	 * Returns radio markup
	 *
	 * @param $_param
	 *
	 * @return bool|string
	 */
	public static function radio( $_param ) {

		return $this->options( $_param );
	}

	/**
	 * Returns checkbox markup
	 *
	 * @param $_param
	 *
	 * @return bool|string
	 */
	public static function checkbox( $_param ) {

		return $this->options( $_param );
	}

	public static function get_value( $field_id, $field_name ) {

		if ( empty( $this->metadata ) ) {
			return FALSE;
		}

		$value = FALSE;
		$id    = str_replace( array( '[', ']' ), '', $field_id );

		if ( array_key_exists( $id, $this->metadata ) && array_key_exists( $field_name, $this->metadata[ $id ] ) ) {

			$value = $this->metadata[ $id ][ $field_name ];

		}

		return $value;

	}

}
