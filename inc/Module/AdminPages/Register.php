<?php # -*- coding: utf-8 -*-
namespace wdmAdminFreezer\Module\AdminPages;

class Register implements RegisterInterface {

	private $callbackClass = '';

    public $currentConfig = [];

	public function init(array $configs) {

		if(!empty($configs)){

			foreach ($configs as $config){
				if ( $this->is_validateSettings( $config ) ) {
					add_action( ( ! empty( $config[ 'network_page' ] ) ? 'network_admin_menu' : 'admin_menu' ), function () use ( $config ) {

						$pageType = ! empty( $config[ 'type' ] ) ? $config[ 'type' ] : 'add_management_page';

						if ( function_exists( $pageType ) ) {
							$this->currentConfig = $config;

							if ( $pageType !== 'add_submenu_page' ) {
								$pageType(
									__( $this->is_validateConfig( $config[ 'title' ] ), 'wdmAdminFreezer'),
									__( $this->is_validateConfig( $config[ 'menu_title' ] ), 'wdmAdminFreezer'),
									$this->is_validateConfig( $config[ 'capability' ] ),
									$this->is_validateConfig( $config[ 'menu_slug' ] ),
									[ $this, 'registerSettings' ]
								);
							} else {
								$pageType(
									$this->is_validateConfig( $config[ 'parent_menu_slug' ] ),
									__( $this->is_validateConfig( $config[ 'title' ] ), 'wdmAdminFreezer'),
									__( $this->is_validateConfig( $config[ 'menu_title' ] ), 'wdmAdminFreezer'),
									$this->is_validateConfig( $config[ 'capability' ] ),
									$this->is_validateConfig( $config[ 'menu_slug' ] ),
									[ $this, 'registerSettings' ]
								);
							}
						}
					} );
				}
			}
		}
	}

	public function registerSettings() {

		$slug           = $this->is_validateConfig( $this->currentConfig[ 'menu_slug' ] );
		$settingsConfig = $this->is_validateConfig( $this->currentConfig[ 'settingsApiConfig' ] );

		if ( ! empty( $settingsConfig ) ) {
			foreach ( $settingsConfig as $settings ) {
				$this->callbackClass = __namespace__ . '\\Callbacks\\' . $this->currentConfig[ 'callback' ];

				if ( class_exists( $this->callbackClass ) ) {

					$sectionSlug   = $this->is_validateConfig( $settings[ 'slug' ] );
					$sectionTitle  = $this->is_validateConfig( $settings[ 'title' ] );
					$sectionFields = $this->is_validateConfig( $settings[ 'add_settings_fields' ] );

					add_settings_section(
						$sectionSlug,
						$sectionTitle,
						'',
						$slug
					);

					foreach ( $sectionFields as $field ) {
						add_settings_field(
							$field['id'],
							__( $field['title'], 'wdmAdminFreezer'),
							[ $this->callbackClass, $field['formType'] ],
							$slug,
							$sectionSlug,
							$field
						);
						register_setting( $slug, $field['id'] );
					}

				register_setting( $slug, $sectionSlug );
				$this->displayPage();

				}
			}
		}
	}

	public function displayPage(){
			( new $this->callbackClass() )->displayPage($this->currentConfig);
	}

    private function is_validateConfig($key){
	    try {
		    if(empty($key)){
			    throw new \Exception("Configuration dose not exists!");
		    }else{
		    	return $key;
		    }
	    } catch (\Exception $e) {
		    echo '<pre>';print_r( ['Exception' => [
			    'msg' => $e->getMessage(),
			    'where' => __CLASS__ . ':' . __LINE__ ,
			    'current_config' => $this->currentConfig
		    ]] );
		    die();
	    }
    }

	private function is_validateSettings($config){
		$neededStack = [
			"title",
            "menu_title",
            "capability",
            "parent_menu_slug",
            "menu_slug",
            "callback",
            "settingsApiConfig"
		];

		foreach($neededStack as $needed) {

			try {
				if ( empty( $config[$needed] ) ) {
					throw new \Exception( "'". $needed . "' is a missing config" );
				} else {
					return $config;
				}
			}
			catch ( \Exception $e ) {
				echo '<pre>';
				print_r( [
					         'Exception' => [
						         'msg'      => $e->getMessage(),
						         'where'    => __CLASS__ . ':' . __LINE__,
						         '$config' => $config
					         ]
				         ] );
				die();
			}
		}
	}
}
