<?php # -*- coding: utf-8 -*-

namespace wdmAdminFreezer\Factories\Abstracts;

use wdmAdminFreezer\Factories\Interfaces;

abstract class Factory implements Interfaces\Factory {

	/**
	 * @var array
	 */
	private $entities = [];

    public function add(string $id, array $args) {
		$this->entities[ $id ] = $args;
	}

	/**
	 * @return array
	 */
	public function get(): array{
		return $this->entities;
	}

	/**
	 * @return array
	 */
	public function unset(string $id): array{
		unset($this->entities[ $id ]);
		return $this->entities;
	}

    public function initialize(string $entity, string $path, string $namespace____, bool $is_static = true){
        $entityFolder = ucfirst($entity);

        $path = $path . '/' . $entityFolder;

        if(is_dir($path)){
            $tasks = glob($path.'/*.php');

            if(!empty($tasks)) {
                foreach ( $tasks as $task ) {
                    require_once( $task );
                    $id = str_replace('.php', '', basename($task));
                    $class =  $namespace . '\\' . $entityFolder . '\\' . $id;

                    if($is_static === true ){
                        $args[$id] = $class;
                    }else {
                        $args = [ $id => (new $class())->$entity() ];
                    }

                    $this->add($entity, $args);
                }
            }
        }

    }
}
