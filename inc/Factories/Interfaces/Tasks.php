<?php # -*- coding: utf-8 -*-

namespace  wdmAdminFreezer\Factories\Interfaces;

interface Tasks{

	public function tasks();

	public function prepareTask();

}
